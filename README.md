# HOA ISP

Repository degli incontri tenutisi nell'Aula 1 del terzo piano del Conservatorio di Musica, Santa Cecilia di Roma sull'_High Order Ambisonic_ ed il progetto _Il Suono di Piero_.



## Gli incontri

Di seguito sono riportati dei link che si riferiscono al diario di bordo volto alla rinascita e l'utilizzo permanente de _Il Suono di Piero_.

- [sabato 18 marzo 2023](/20230318)
- [sabato 25 marzo 2023](/20230325)
